# ComdainPDF

[![CI Status](https://img.shields.io/travis/GgnSwaitch/ComdainPDF.svg?style=flat)](https://travis-ci.org/GgnSwaitch/ComdainPDF)
[![Version](https://img.shields.io/cocoapods/v/ComdainPDF.svg?style=flat)](https://cocoapods.org/pods/ComdainPDF)
[![License](https://img.shields.io/cocoapods/l/ComdainPDF.svg?style=flat)](https://cocoapods.org/pods/ComdainPDF)
[![Platform](https://img.shields.io/cocoapods/p/ComdainPDF.svg?style=flat)](https://cocoapods.org/pods/ComdainPDF)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ComdainPDF is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ComdainPDF'
```

## Author

GgnSwaitch, gswaitch@comdain.com.au

## License

ComdainPDF is available under the MIT license. See the LICENSE file for more info.
