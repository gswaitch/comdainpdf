//
//  GAppDelegate.h
//  ComdainPDF
//
//  Created by GgnSwaitch on 05/04/2018.
//  Copyright (c) 2018 GgnSwaitch. All rights reserved.
//

@import UIKit;

@interface GAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
