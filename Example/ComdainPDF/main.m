//
//  main.m
//  ComdainPDF
//
//  Created by GgnSwaitch on 05/04/2018.
//  Copyright (c) 2018 GgnSwaitch. All rights reserved.
//

@import UIKit;
#import "GAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GAppDelegate class]));
    }
}
